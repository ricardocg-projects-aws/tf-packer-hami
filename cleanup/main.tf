variable "ami_id" {
    description = "ami id obtained from the packer output end exported to TF variable"
}

resource "null_resource" "delete_snapshot" {
    provisioner "local-exec" {
        command = "python3 cleanup.py ${var.ami_id}"
    }
}