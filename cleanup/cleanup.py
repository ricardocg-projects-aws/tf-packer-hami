import sys
import boto3

image_id = sys.argv[1]

client = boto3.client('ec2')

volumes = []

image = client.describe_images(
    ImageIds=[image_id]
)

# Grab EBS snapshots associated with AMI then deregister AMI then delete snapshots
for volume in image['Images'][0]['BlockDeviceMappings']:
    print(volume['Ebs']['SnapshotId'])
    volumes.append(volume['Ebs']['SnapshotId'])

print(client.deregister_image(
   ImageId= image_id
))

for snapshot_id in volumes:
    print(client.delete_snapshot(
        SnapshotId=snapshot_id
    ))